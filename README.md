# Thinkific Test - Nathan Shubert-Harbison

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Run the assignment

1. `cd` into the project directory and run `yarn install` to install dependencies.
2. Start the development server with `yarn start`.

## Features
- Displays weather for Vancouver, BC
- Different days can be selected for a full day view
- In the day view, the dots on the graph as well as times in the legend can be clicked to see more details for that timeframe.

## Assumptions & Decisions

- The app uses https for api/dev server protocol agreement, but will require accepting the security notice locally
- Given that Open Weather Map [only returns forecasts based on GMT](https://openweathermap.desk.com/customer/portal/questions/16864352-getting-weather-data-for-different-time-zones), there is some funkiness. Ideally I would want to show 5 complete days, starting from the beginning of the current day. Since the API returns forecasts for "now, in GMT", the current day is sometimes incomplete. For this reason I defaulted to showing the second day, to display a full day in view.
- In order to represent a whole day with one weather icon, I represent the daily forecast with an icon taken from weather at mid-day. I also default to mid-day in the full day view