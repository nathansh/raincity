export const fonts = {
    base: 'Noto Sans JP',
};

export const colors = {
    base: '#fff',
};

export const layout = {
    maxWidth: 1020,
    gutter: 30,
    breakpoint: 620,
};

export const fontWeights = {
    light: 300,
    regular: 400,
    medium: 500,
    bold: 700,
};