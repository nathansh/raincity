export const appText = {
    title: 'RainCity',
};

export const weather = {
    error: 'Looks like there\'s a problem loading weather data at the moment 😰',
};