export const groupForecastsByDays = (forecast) => {
    const days = {};

    forecast.forEach((item) => {
        const day = item.dt_txt.split(' ')[0];

        if (!days[day]) {
            days[day] = [];
        }

        days[day].push(item);
    });

    return days;
};

export const midDayIndex = (day) => {
    return day.length > 1 ? Math.ceil(day.length / 2) : 0;
};

export const dailyHigh = (day) => {
    return day.reduce((accumulator, currentValue) => {
        return accumulator > currentValue.main.temp ? accumulator : currentValue.main.temp;
    }, 0);
};

export const dailyLow = (day) => {
    return day.reduce((accumulator, currentValue) => {
        return accumulator < currentValue.main.temp ? accumulator : currentValue.main.temp;
    }, 100);
};