const errorMessagge = 'RainCity: Could not load data 🤔';

const handleError = () => {
    console.error(errorMessagge);
};

const getData = ((requestUrl) => {
    return new Promise((resolve, reject) => {
        fetch(requestUrl)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    handleError();
                }
            })
            .then((data) => {
                resolve(data);
            })
            .catch((error) => {
                handleError();
                reject(errorMessagge);
            });
    });
});

export default getData;