import PropTypes from 'prop-types';

export const forecast = {
    clouds: PropTypes.shape({
        all: PropTypes.number,
    }),
    dt: PropTypes.number,
    dt_txt: PropTypes.string,
    main: PropTypes.shape({
        grnd_level: PropTypes.number,
        humidity: PropTypes.number,
        pressure: PropTypes.number,
        sea_level: PropTypes.number,
        temp: PropTypes.number,
        temp_kf: PropTypes.number,
        temp_max: PropTypes.number,
        temp_min: PropTypes.number,
    }),
    sys: PropTypes.shape({
        pod: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]),
    }),
    weather: PropTypes.arrayOf(
        PropTypes.shape({
            description: PropTypes.string,
            icon: PropTypes.string,
            id: PropTypes.number,
            main: PropTypes.string,
        })
    ),
    wind: PropTypes.shape({
        deg: PropTypes.number,
        speed: PropTypes.number,
    }),
};