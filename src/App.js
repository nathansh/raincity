import React from 'react';
import { connect } from 'react-redux';
import 'normalize.css';
import styled, {
    createGlobalStyle,
} from 'styled-components';
import { loadWeather } from 'store/actions';
import { appText as text } from 'lib/text';
import {
    fonts,
    colors,
} from 'lib/style';

import Header from 'components/Header';
import Container from 'components/layout/Container';
import DayView from 'components/DayView';
import DayNav from 'components/DayNav';
import SkeletonScreen from 'components/SkeletonScreen';
import ErrorMessage from 'components/ErrorMessage';

const GlobalStyle = createGlobalStyle`
    html {
        min-height: 100%;
    }
    body {
        font-family: ${fonts.base}, sans-serif;
        background-image: linear-gradient(#645084, #7d3456);
        min-height: 100%;
        color: ${colors.base};
    }
    * {
        box-sizing: border-box;
    }
    .recharts-cartesian-axis-tick {
        cursor: pointer;
    }
`;

const NavWrapper = styled.div`
    margin-top: 45px;
`;

const App = (props) => {
    return (
        <>
            <GlobalStyle />
            <Header title={text.title} />
            <Container>
                {props.forecasts ? (
                    <>
                        <DayView />
                        <NavWrapper>
                            <DayNav days={props.forecasts}/>
                        </NavWrapper>
                    </>
                ) : (
                    props.error ? (
                        <ErrorMessage>
                            {props.error}
                        </ErrorMessage>
                    ) : (
                        props.loadWeather(),
                        <SkeletonScreen />
                    )
                )}
            </Container>
        </>
    );
};

const mapStateToProps = (state) => ({
    forecasts: state.weather,
    error: state.weatherError,
});

const mapDispatchToProps = (dispatch) => ({
    loadWeather() {
        dispatch(
            loadWeather()
        );
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);