module.exports = {
    "extends": "react-app",
    "rules" : {
        "indent": ["error", 4],
        "comma-dangle": ["error", "always-multiline"],
        "semi": [
            "error",
            "always", {
                "omitLastInOneLineBlock": true
            }
        ]
    }
}