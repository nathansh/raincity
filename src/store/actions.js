import actionTypes from 'store/actionTypes';
import getData from 'lib/getData';
import { weather as weatherText } from 'lib/text';
import { API_URL } from 'lib/constants';

export const updateWeather = (weatherData) => {
    return {
        type: actionTypes.WEATHER__SET,
        payload: weatherData,
    };
};

export const setErrorMessage = (message) => {
    return {
        type: actionTypes.ERROR__SET,
        payload: message,
    };
};

export const setCurrentDay = (day) => {
    return {
        type: actionTypes.WEATHER__SET_CURRENT_DAY,
        payload: day,
    };
};

export const setCurrentHour = (day) => {
    return {
        type: actionTypes.WEATHER__SET_CURRENT_HOUR,
        payload: day,
    };
};

export const loadWeather = () => {
    return (dispatch) => {
        getData(API_URL)
            .then((data) => {
                dispatch(
                    updateWeather(data.list)
                );
            })
            .catch((error) => {
                dispatch(
                    setErrorMessage(weatherText.error)
                );
                console.error(error);
            });
    };
};