import { combineReducers } from 'redux';
import actionTypes from 'store/actionTypes';
import {
    groupForecastsByDays,
    midDayIndex,
} from 'lib/utils';

const defaultDayIndex = 1;

const weather = (state = null, action) => {
    return action.type === actionTypes.WEATHER__SET ? groupForecastsByDays(action.payload) : state;
};

const currentDay = (state = null, action) => {
    if (action.type === actionTypes.WEATHER__SET) {
        const grouped = groupForecastsByDays(action.payload);
        return grouped[Object.keys(grouped)[defaultDayIndex]];
    } else if (action.type === actionTypes.WEATHER__SET_CURRENT_DAY) {
        return action.payload;
    } else {
        return state;
    }
};

const currentHour = (state = null, action) => {
    if (action.type === actionTypes.WEATHER__SET) {
        const grouped = groupForecastsByDays(action.payload);
        const defaultDay = grouped[Object.keys(grouped)[defaultDayIndex]];
        const hourIndex = midDayIndex(defaultDay);
        return defaultDay[hourIndex];
    } else if (action.type === actionTypes.WEATHER__SET_CURRENT_DAY ) {
        const hourIndex = midDayIndex(action.payload);
        return action.payload[hourIndex];
    } else if (action.type === 'WEATHER__SET_CURRENT_HOUR') {
        return action.payload;
    } else {
        return state;
    }
};

const city = (state = 'Vancouver, BC', action) => {
    return action.type === actionTypes.CITY__SET ? action.payload : state;
};

const weatherError = (state = null, action) => {
    return action.type === actionTypes.ERROR__SET ? action.payload : state;
};

export default combineReducers({
    weather,
    weatherError,
    currentDay,
    currentHour,
    city,
    lastAction: (state = null, action) => {
        return action;
    },
});