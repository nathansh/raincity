import thunk from 'redux-thunk';
import appReducer from './reducers';
import {
    createStore,
    applyMiddleware,
    compose,
} from 'redux';

const withDevTools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default (initialState = {}) => {
    return createStore(
        appReducer,
        initialState,
        withDevTools(applyMiddleware(thunk)),
    );
};