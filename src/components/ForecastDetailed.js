import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import { forecast as forecastShape } from 'lib/propShapes';

import WeatherIconTile from 'components/WeatherIconTile';
import ForecastDetailIcons from 'components/ForecastDetailIcons';

import {
    layout,
    fontWeights,
} from 'lib/style';

const DetailsWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
`;

const IconsWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
`;

const IconItem = styled.div`
    padding: ${layout.gutter / 2}px ${layout.gutter}px;
    display: flex;
    flex-direction: row;
    @media (max-width: ${layout.breakpoint}px) {
        padding: ${layout.gutter / 2}px;
    }
`;

const Day = styled.h3`
    font-size: 60px;
    font-weight: ${fontWeights.light};
    margin: 0 0 .5em;
    text-align: center;
    @media (max-width: ${layout.breakpoint}px) {
        font-size: 30px;
        margin-bottom: 1.5em;
    }
`;

const City = styled.h2`
    font-size: 18px;
    font-weight: ${fontWeights.light};
    text-transform: uppercase;
    margin: 0 0 .5em;
    text-align: center;
`;

const Temperature = styled.span`
    font-size: 82px;
    font-weight: ${fontWeights.light};
    line-height: 60px;
    @media (max-width: ${layout.breakpoint}px) {
        font-size: 50px;
        line-height: 40px;
    }
`;

const ForecastDetailed = (props) => {
    const day = moment.utc(props.currentForecast.dt * 1000);
    const dateFormat = 'dddd, MMM Do';

    return (
        <div>
            <DetailsWrapper>
                <City>
                    {props.city}
                </City>
                <Day>
                    {day.format(dateFormat)}
                </Day>
                <IconsWrapper>
                    <IconItem>
                        <WeatherIconTile
                            date={false}
                            size='large'
                            forecast={props.forecast}
                            description
                        />
                    </IconItem>
                    <IconItem>
                        <Temperature>
                            {Math.round(props.forecast.main.temp)}
                        </Temperature>
                        <span>°C</span>
                    </IconItem>
                </IconsWrapper>
            </DetailsWrapper>
            <ForecastDetailIcons forecast={props.forecast} />
        </div>
    );
};

ForecastDetailed.defaultProps = {
    dateFormat: 'dddd, MMM Do',
};

ForecastDetailed.propTypes = {
    city: PropTypes.string.isRequired,
    forecast: PropTypes.shape(forecastShape),
    dateFormat: PropTypes.string,
};

const mapStateToProps = (state) => ({
    forecast: state.currentHour,
    currentForecast: state.currentHour,
});

export default connect(mapStateToProps)(ForecastDetailed);