import React from 'react';
import styled, { keyframes } from 'styled-components';
import { layout } from 'lib/style';

const blink = keyframes`
    0% {
        opacity: .5;
    }

    15% {
        opacity: 1;
    }

    100% {
        opacity: .5;
    }
`;

const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-top: 50px;
`;

const Box = styled.div`
    background: rgba(255, 255, 255, .1);
    animation: ${blink} 2s ease-in-out infinite;
    border-radius: 3px;
    @media (max-width: ${layout.breakpoint}px) {
        width: 100%;
    }
`;

const City = styled(Box)`
    height: 30px;
    margin-bottom: 20px;
    width: 30%
`;

const Day = styled(Box)`
    height: 50px;
    width: 60%;
    margin-bottom: 50px;
`;

const Forecast = styled(Box)`
    height: 50vh;
    width: 100%;
`;

const SkeletonScreen = (props) => (
    <Container>
        <City />
        <Day />
        <Forecast />
    </Container>
);

export default SkeletonScreen;