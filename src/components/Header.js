import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
    fontWeights,
    layout,
} from 'lib/style';

import WeatherIcon from 'components/WeatherIcon';
import Container from 'components/layout/Container';

const HeaderWrapper = styled.div`
    padding: 50px 0;
    @media (max-width: ${layout.breakpoint}px) {
        text-align: center;
        padding: 25px 0;
    }
`;

const Title = styled.h1`
    font-size: 32px;
    font-weight: ${fontWeights.light};
    margin: 0;
    transform: translateY(-3px);
`;

const Logo = styled.span`
    display: inline-block;
    margin-right: 15px;
    transform: translateY(-3px);
`;

export const Header = (props) => (
    <Container>
        <HeaderWrapper>
            <Title>
                <Logo>
                    <WeatherIcon name='rain' />
                </Logo>
                {props.title}
            </Title>
        </HeaderWrapper>
    </Container>
);

Header.propTypes = {
    title: PropTypes.string.isRequired,
};

export default Header;