import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { layout } from 'lib/style';

const StyledConotainer = styled.div`
    margin: 0 auto;
    max-width: ${layout.maxWidth + layout.gutter}px;
    padding-left: ${layout.gutter / 2}px;
    padding-right: ${layout.gutter / 2}px;
    ${props => props.center && `
        text-align: center;
    `}
`;

const Container = (props) => (
    <StyledConotainer
        center={props.center}
    >
        {props.children}
    </StyledConotainer>
);

Container.propTypes = {
    center: PropTypes.bool,
    spaceAround: PropTypes.bool,
};

export default Container;