import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { forecast as forecastShape } from 'lib/propShapes';

import ForecastDetailed from 'components/ForecastDetailed';
import ForecastTempChart from 'components/ForecastTempChart';

const DayView = (props) => {
    return (
        <div>
            <ForecastDetailed city={props.city} />
            <ForecastTempChart forecast={props.forecast} />
        </div>
    );
};

DayView.propTypes = {
    forecast: PropTypes.arrayOf(
        PropTypes.shape(forecastShape)
    ).isRequired,
};

const mapStateToProps = (state) => ({
    forecast: state.currentDay,
    city: state.city,
});

export default connect(mapStateToProps)(DayView);