import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import WeatherIcon from 'react-icons-weather';

import { forecast as forecastShape } from 'lib/propShapes';
import {
    dailyHigh,
    dailyLow,
} from 'lib/utils';

import {
    layout,
    fontWeights,
} from 'lib/style';

const TileContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const IconContainer = styled.div`
    font-size: ${props => props.size === 'large' ? 60 : 30}px;
    @media (max-width: ${layout.breakpoint}px) {
        ${props => props.size === 'large' &&`
            font-size: 40px;
        `}
    }
`;

const TileDate = styled.div`
    font-size: 16px;
    font-weight: ${fontWeights.regular};
    margin-bottom: 1em;
`;

const TemperatureRange = styled.div`
    display: flex;
    margin-top: 10px;
`;

const Temperature = styled.span`
    padding: 0 5px;
    display: inline-block;
`;

const LowTemperature = styled(Temperature)`
    opacity: .5;
`;

const WeatherIconTile = (props) => {
    if (!props.forecast) {
        return null;
    }

    const day = moment.utc(props.forecast.dt * 1000);
    const dateFormat = 'dddd';
    const description = props.forecast.weather[0].description;
    const titleCaseDescription = description.charAt(0).toUpperCase() + description.slice(1);

    return (
        <TileContainer>
            {props.date ? (
                <TileDate>
                    {day.format(dateFormat)}
                </TileDate>
            ) : (null)}

            <IconContainer size={props.size}>
                <WeatherIcon
                    name='owm'
                    iconId={`${props.forecast.weather[0].id}`}
                />
            </IconContainer>

            {props.description ? (
                <div>{titleCaseDescription}</div>
            ) : (null)}

            {props.temperatures && props.fullDay ? (
                <TemperatureRange>
                    <Temperature>
                        {Math.round(dailyHigh(props.fullDay))}
                        <span>°</span>
                    </Temperature>
                    <LowTemperature>
                        {Math.round(dailyLow(props.fullDay))}
                        <span>°</span>
                    </LowTemperature>
                </TemperatureRange>
            ) : (null)}
        </TileContainer>
    );
};

WeatherIconTile.defaultProps = {
    date: true,
    size: 'small',
    temperatures: false,
    description: false,
};

WeatherIconTile.propTypes = {
    date: PropTypes.bool,
    size: PropTypes.oneOf([
        'small',
        'large',
    ]),
    temperatures: PropTypes.bool,
    description: PropTypes.bool,
    forecast: PropTypes.shape(forecastShape),
    fullDay: PropTypes.arrayOf(
        PropTypes.shape(forecastShape),
    ),
};

export default WeatherIconTile;