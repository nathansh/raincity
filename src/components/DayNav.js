import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { forecast as forecastShape } from 'lib/propShapes';
import { midDayIndex } from 'lib/utils';
import { setCurrentDay } from 'store/actions';

import WeatherIconTile from 'components/WeatherIconTile';

import {
    colors,
    layout,
} from 'lib/style';

const Nav = styled.ul`
    list-style: none;
    padding: 0;
    margin: 0;
    display: flex;
    justify-content: space-around;
    @media (max-width: ${layout.breakpoint}px) {
        width: calc(100% + ${layout.gutter}px);
        padding: 0 ${layout.gutter}px;
        margin-left: -${layout.gutter / 2}px;
        overflow-x: scroll;
        -webkit-overflow-scrolling: touch;
    }
`;

const Item = styled.li`
    display: flex;
    flex-grow: 1;
    flex-basis: 0;
    justify-content: center;
`;

const DayButton = styled.button`
    cursor: pointer;
    background: ${props => props.active ? 'rgba(255, 255, 255, .15)' : 'none'};
    border: 0;
    color: ${colors.base};
    outline: 0;
    padding: 15px;
    border-radius: 3px;
`;

const DayNav = (props) => {
    const currentMidDayIndex = midDayIndex(props.currentDay);
    const currentMidDay = props.currentDay[currentMidDayIndex];

    return (
        <Nav>
            {Object.keys(props.days).slice(0, 5).map((day, index) => {
                const hour = midDayIndex(props.days[day]);

                return (
                    <Item key={`day-${index}`}>
                        <DayButton
                            onClick={() => props.setDay(props.days[day])}
                            active={props.days[day][hour].dt === currentMidDay.dt}
                        >
                            <WeatherIconTile
                                temperatures
                                forecast={props.days[day][hour]}
                                fullDay={props.days[day]}
                            />
                        </DayButton>
                    </Item>
                );
            })}
        </Nav>
    );
};

DayNav.propTypes = {
    days: PropTypes.shape({
        [PropTypes.string]: PropTypes.shape(forecastShape),
    }),
};

const mapStateToProps = (state) => ({
    currentDay: state.currentDay,
});

const mapDispatchToProps = (dispatch) => ({
    setDay(day) {
        dispatch(
            setCurrentDay(day)
        );
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(DayNav);