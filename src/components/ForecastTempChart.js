import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import {
    LineChart,
    Line,
    YAxis,
    XAxis,
    ResponsiveContainer,
} from 'recharts';
import { forecast as forecastShape } from 'lib/propShapes';
import { colors } from 'lib/style';
import { setCurrentHour } from 'store/actions';

const Dot = styled.svg`
    cursor: pointer;
`;

const handleDotClick = (props) => {
    props.setCurrentHour(props.forecast[props.payload.index]);
};

const handleLegendClick = (data, props) => {
    props.setCurrentHour(props.forecast[data.index]);
};

const PointDot = (props) => {
    const size = props.currentHour.dt === props.payload.timestamp ? 20 : 10;

    return (
        <Dot
            onClick={() => handleDotClick(props)}
            x={props.cx - (size / 2)}
            y={props.cy - (size / 2)}
            width={size}
            height={size}
            fill='white'
            viewBox='0 0 100 100'
        >
            <circle
                cx='50'
                cy='50'
                r='50'
            />
        </Dot>
    );
};

const ForecastTempChart = (props) => {
    const temperatures = props.forecast.map((forecast, index) => {
        const time = moment.utc(forecast.dt * 1000);

        return {
            timestamp: forecast.dt,
            time: time.format('ha'),
            temperature: forecast.main.temp,
            index: index,
        };
    });

    return (
        <div>
            <ResponsiveContainer aspect={4}>
                <LineChart
                    width={300}
                    height={100}
                    data={temperatures}
                    margin={{
                        right: 20,
                        top: 10,
                    }}
                >
                    <Line
                        type='monotone'
                        dataKey='temperature'
                        stroke={colors.base}
                        strokeWidth={2}
                        dot={
                            <PointDot
                                forecast={props.forecast}
                                setCurrentHour={props.setCurrentHour}
                                currentHour={props.currentHour}
                            />
                        }
                    />
                    <YAxis
                        stroke={colors.base}
                    />
                    <XAxis
                        stroke={colors.base}
                        dataKey='time'
                        onClick={(data) => handleLegendClick(data, props)}
                    />
                </LineChart>
            </ResponsiveContainer>
        </div>
    );
};

ForecastTempChart.propTypes = {
    forecast: PropTypes.arrayOf(
        PropTypes.shape(forecastShape),
    ),
};

const mapStateToProps = (state) => ({
    currentHour: state.currentHour,
});

const mapDispatchToProps = (dispatch) => ({
    setCurrentHour(time) {
        dispatch(
            setCurrentHour(time)
        );
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(ForecastTempChart);