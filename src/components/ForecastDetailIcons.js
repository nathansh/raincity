import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { forecast as forecastShape } from 'lib/propShapes';
import { layout } from 'lib/style';

import WeatherIcon from 'components/WeatherIcon';

const Wrapper = styled.div`
    display: flex;
    justify-content: center;
    margin: ${layout.gutter}px;
`;

const Item = styled.div`
    display: flex;
    padding: 0 ${layout.gutter / 2}px
`;

const Icon = styled.div`
    font-size: 22px;
    margin: 0 15px;
`;

const Value = styled.div`
    font-size: 18px;
    line-height: 22px;
`;

const ForecastDetailIcons = (props) => {
    return (
        <Wrapper>
            <Item>
                <Icon>
                    <WeatherIcon name='windy' />
                </Icon>
                <Value>
                    {Math.round(props.forecast.wind.speed)} km/h
                </Value>
            </Item>

            <Item>
                <Icon>
                    <WeatherIcon name='humidity' />
                </Icon>
                <Value>
                    {props.forecast.main.humidity}%
                </Value>
            </Item>
        </Wrapper>
    );
};

ForecastDetailIcons.propTypes = {
    forecast: PropTypes.shape(forecastShape),
};

export default ForecastDetailIcons;