import React from 'react';
import styled from 'styled-components';
import {
    fontWeights,
    layout,
} from 'lib/style';

const StyledError = styled.div`
    text-align: center;
    font-size: 38px;
    width: 75%;
    margin: 0 auto;
    line-height: 1.5em;
    font-weight: ${fontWeights.light};
    padding-top: 15vh;
    @media (max-width: ${layout.breakpoint}px) {
        font-size: 28px;
        pading-top: 5vh;
    }
`;

const ErrorMessage = (props) => (
    <StyledError>
        {props.children}
    </StyledError>
);

export default ErrorMessage;